
import { KEY, getValue } from '@storage/AsyncStorageData'
// import NetInfo from "@react-native-community/netinfo";

import axios from "axios";
import { Alert } from 'react-native';
// import { APP_NAME } from '../../../storage/HelpherFile';

// import { showToast } from '../../../utils/ToastMsg';

class NetworkUtils {
  constructor(options) {
    this.baseUrl = options.baseUrl;
  }

  get(endpoint, token = null) {
    return this.requestHttp("GET", this.baseUrl + endpoint, null);
  }

  post(endpoint, params, token = null) {

    return this.requestHttp("POST", this.baseUrl + endpoint, params);
  }

  put(endpoint, params, token = null) {
    return this.requestHttp("PUT", this.baseUrl + endpoint, params);
  }

  delete(endpoint, params, token = null) {
    return this.requestHttp("DELETE", this.baseUrl + endpoint, params);
  }


  // getInternetStatus = () => {
  //   let lStatus = false
  //   NetInfo.addEventListener(state => {
  //     lStatus = state.isConnected
  //     if(!state.isConnected){
  //       Alert.alert("","Please check your internet connection","default")
  //     }
  //   });
  //   return lStatus
  // }
  async requestHttp(method, url, params) {
    // let isConnected = this.getInternetStatus();
    // debugger;
    let auth_token=null;
    let data = await getValue(KEY.USER_DATA);
    if(data){
let lData=JSON.parse(data)
auth_token=lData?.token
    }

    var formData = new URLSearchParams();// FormData();
    if (params) {
      for (var key in params) {
        // if (key == "mobile_number") {
        //   formData.append("mobile_number[]", JSON.stringify(params[key]))
        // }
        // else if (key == "resume") {
        //   formData.append("resume[]", JSON.stringify(params[key]))
        // }
        // else{  
          formData.append(key, params[key]);
        // }
      }
    }

    return new Promise((resolve, reject) => {
      var options = {
        method,
        url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + auth_token,
          // 'Content-Type':'multipart/form-data'
        },
        data:params? JSON.stringify(params):undefined
      };
      
      debugger;
      axios(options)
        .then(response => {
          
          resolve({ statusCode: response.status, body: response.data });
        })
        .catch(error => {
          debugger;
          console.log(error)
          if (error.response != undefined) {
            resolve({
              statusCode: error.response.status,
              body: error.response.data
            });
          } else {
            // reject(__.t("Can not connect to server"));
            resolve({ statusCode: 503, body: null });
            Alert.alert("Server Error!", "Can not connect to server")
          }
        });
    });
  }
}

export default NetworkUtils;
