import Api from "./Api";
export const postGenericService = (url,param,contenType="form-data") => {
    return new Promise((resolve, reject) => {
        // debugger;
      Api.post(url,param,contenType)
        .then(response => {
          // console.log(response);
          debugger
          if (response.statusCode == 200) {
            resolve(response.body);
          } 
          else  if (response.statusCode == 204) {
            resolve(response.body);
          }
          else if(response.statusCode==503)
          {
            resolve(response.body)
          }
          else if(response.statusCode==401){
            resolve(response.body)
          }
          else if(response.statusCode==422){
            resolve(response.body)
          }
          else if(response.statusCode==500){
            resolve(response.body)
          }
          else if(response.statusCode==404){
            resolve(response.body)
          }

          else {
            var params = response.body.parameters;
            if (typeof params != "undefined" && params.length > 0) {
              var message = response.body.message;
              params.forEach((item, index) => {
                message = message.replace("%" + (index + 1), item);
              });
              reject(message);
            } else {
              reject(response.body.message);
            }
          }
        })
        .catch(reject);
    });
  };

  export const getGenericService = (url,param={},contenType="form-data") => {
    return new Promise((resolve, reject) => {
        // debugger;
      Api.get(url,param,contenType)
        .then(response => {
          debugger
          if (response.statusCode == 200) {
            resolve(response.body);
          } 
          else  if (response.statusCode == 204) {
            resolve(response.body);
          }
          else  if (response.statusCode == 404) {
            resolve(response.body);
          }
          else {
            var params = response.body.parameters;
            if (typeof params != "undefined" && params.length > 0) {
              var message = response.body.message;
              params.forEach((item, index) => {
                message = message.replace("%" + (index + 1), item);
              });
              reject(message);
            } else {
              reject(response.body.message);
            }
          }
        })
        .catch(reject);
    });
  };
