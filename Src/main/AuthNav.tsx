import React, { useEffect } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from '../Screens/auth/LoginScreen';
// import RegisterScreen from '../Screens/auth/RegisterScreen';
// import ForgetEmail from '../Screens/auth/ForgetEmail';
// import OtpVerification from '../Screens/auth/OtpVerification';
// import IntroScreen from '../Screens/Intro/IntroScreen';
import SplashScreen from '../Screens/Intro/SplashScreen';
import { connect } from 'react-redux';
import { StackNav } from './StackNav'
import { getUserData } from '../redux/actions/AuthAction'
import { RootState } from '../redux/store/Store';
import{View,StatusBar} from 'react-native'
// import ForgetPassword from '../Screens/auth/ForgetPassword';
import LogoutScreen from '../Screens/auth/LogoutScreen';


const AuthStack = createNativeStackNavigator();
// const AuthStack = createStackNavigator<StackParamList>();

function AuthNav(props: any) {
    useEffect(() => {
        debugger
        // props.getUserData()
    }, [])
    const { username, token,email, password } = props
    return (
    //     ((username && password)) ?
       <>
       <StatusBar backgroundColor="#2E75B6" barStyle="light-content" />
       {/* {(email && token) ? */}
        {(email) ?
            <StackNav />
            
            :
            <AuthStack.Navigator
                screenOptions={{ headerShown: false }}
                initialRouteName="SplashScreen">
                <AuthStack.Screen name="SplashScreen" component={SplashScreen} />
                {/* <AuthStack.Screen name="IntroScreen" component={IntroScreen} /> */}
                <AuthStack.Screen name="LoginScreen" component={LoginScreen} />
                {/* <AuthStack.Screen name="RegisterScreen" component={RegisterScreen} />
            <AuthStack.Screen name="OtpVerification" component={OtpVerification} />
            <AuthStack.Screen name="ForgetEmail" component={ForgetEmail} />
            <AuthStack.Screen name="ForgetPassword" component={ForgetPassword} /> */}
        
            </AuthStack.Navigator> }
</>
    );
}
const mapStateToProps = (state: RootState) => {
    return {
        token: state.auth.token,
        username: state.auth.username,
        password:state.auth.password,
        user_data: state.auth.user_data,
        email:state.auth.email
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        getUserData: () => dispatch(getUserData()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthNav)
