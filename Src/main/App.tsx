// In App.js in a new project

import React, { useEffect } from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import AuthNav from './AuthNav'
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import store from '../redux/store/Store';
import { navigationRef, isReadyRef } from './RootNavigation';
import Root, { NativeBaseProvider } from 'native-base'
import {LogBox} from 'react-native'
// import Colors from '../themes/Colors';

LogBox.ignoreLogs(['Warning: ...']);

//Ignore all log notifications
LogBox.ignoreAllLogs();

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#cb29ac',
    
  },
};
function App() {

  useEffect(() => {
    return () => {
      // isReadyRef.current = false
    };
  }, []);

  return (
    <Provider store={store}>
      <NativeBaseProvider>
      <SafeAreaProvider>
        <NavigationContainer ref={navigationRef}
          onReady={() => {
            // isReadyRef.current = true;
          }}
          theme={MyTheme}>
          {/* <StackNav /> */}
          {/* <Root> */}
            <AuthNav />
            {/* </Root> */}
        </NavigationContainer>
      </SafeAreaProvider>
      </NativeBaseProvider>
    </Provider>

  );
}


export default (App)


/*
step 1: create stack navigator
step2:create bottom navigator and use it inside stack navigator
step3: create drawer navigator and use it inside bottom navigator
*/
