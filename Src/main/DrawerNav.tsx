import React, { useState, useEffect,useCallback } from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';

import MainScreen from '../Screens/dashboard/MainScreen'
import { removeUserData, saveUserData, updatePassword, updateUserName } from '../redux/actions/AuthAction'
import { getValue, KEY, setValue } from '../storage/AsyncStorageData';
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import { Text, View , Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { RootState } from '../redux/store/Store';
import {Avatar,Row,Column,Pressable} from "native-base";
import { Colors, Images } from "../Themes";
const Drawer = createDrawerNavigator<DrawerNavParamList>()

interface myobject{
name:string
email:string
profile_path:string
length:string
props:any
}

function CustomDrawerContent(props:any) {
  
const goto=(route:string)=>{
props.navigation.closeDrawer()
  props.navigation.navigate(route)  
}

const _logout=()=>{

// props.updateUserName("")
// props.updatePassword("")
}
  const { name ,email,profile_path,length} = props

  return (
    <DrawerContentScrollView {...props}>
     
      <View style={styles.userDetailsContainer}>
      
        <Image style={{height: 50,width: 50,borderRadius: 40}} 
        source={{uri:'https://ulbha.com/front_assets/images/user.png'}}
        />
        <View style={styles.userCred}>
          <Text style={{ color:'#cb29ac',fontSize: 16,fontWeight: '500',width: 190,}}>{name}</Text>
          <Text style={{ color:'#cb29ac',fontSize: 13,fontWeight: '500',width:190}}>{email}</Text>
        </View>
      </View>
      <DrawerItemList {...props} />

      <DrawerItem
        icon={({ focused, color, size }) => <Ionicons name={'person-circle-outline'} size={28} color="#cb29ac" />}
        label={({ focused, color }) => <Text style={{ color:'#cb29ac',fontSize: 15,fontWeight: '500',}}>Profile</Text>}
        onPress={() =>goto('ProfileScreen')}
      />

      <DrawerItem
        icon={({ focused, color, size }) => <MaterialIcons name={'logout'} size={28} color="#cb29ac" style={{marginLeft: 2,}}/>}
        label={({ focused, color }) => <Text style={{ color:'#cb29ac',fontSize: 15,fontWeight: '500',right: 2,}}>Logout</Text>}
        onPress={() =>goto('Logout')}
      />
    </DrawerContentScrollView>
  );
}

const DrawerMenu = () => {
  const [password, setPassword] = useState<any>(null)
  const [username, setUsername] = useState<any>(null)
  const [email, setEmail] = useState<any>(null)
  const [profile_path,setPath] = useState<any>(null)
  const [notification,setLength] = useState<any>(null)
  const fetchUserData = useCallback(async () => {
    let data = await getValue(KEY.USER_DATA)
    if (data) {
      let lData = JSON.parse(data)
      if(lData){
      setUsername(lData?.name + " " +lData?.last_name);
      setPassword(lData?.password);
      setEmail(lData?.email);
      setPath(lData?.file_path + lData?.user_profile_file);
   
   
    }
    }
    let length = await getValue(KEY.NOTIFICATION)
    if(length){
      setLength(length);
    }
  }, [])

  useEffect(() => {
    fetchUserData()
  }, [fetchUserData])

  return (
    <Drawer.Navigator
      screenOptions={({ route }) => ({
        drawerIcon: ({ focused, color, size }) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = focused
              ? 'home'
              : 'home';
              
          } else {
            iconName = focused ? 'ios-list' : 'ios-list';
          }
          return  <Ionicons name={iconName} size={size} color="#cb29ac" />;
        },
        drawerLabel:({ focused, color}) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = focused
              ? 'Home'
              : 'Home';
              
          } else {
            iconName = focused ? 'ios-list' : 'ios-list';
          }
         return  <Text style={{color:"#cb29ac",fontSize: 15,fontWeight:'500',marginLeft: 5,}} >{iconName}</Text>;
        },
      })}
      initialRouteName="Home" drawerContent={props => <CustomDrawerContent name={username} email={email} profile_path ={profile_path} length={notification} {...props} />} >
      <Drawer.Screen name="Home" component={MainScreen} options={{headerShown:false}} />
      </Drawer.Navigator>
  );
}
const mapStateToProps = (state:RootState) => {
  return {
    username: state.auth.username,
    password: state.auth.password
  }
}

const mapDispatchToProps = (dispatch: (arg0: any) => any)=> {
  return {
    saveUserData: (data:any) => dispatch(saveUserData(data)),
    updateUserName:(data:any)=>dispatch(updateUserName(data)),
    updatePassword:(data:any)=>dispatch(updatePassword(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerMenu)
const styles = StyleSheet.create({
  userDetailsContainer: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 10,

  },
  userInfoView: {
    width: 80,
    height: 80,
    backgroundColor: Colors.DARK_BLUE,
    borderRadius: 100 / 2,
  },
  userCred: {
    justifyContent: 'center',
    paddingLeft: 10,
    color:"black",
    flex:1
  }
})