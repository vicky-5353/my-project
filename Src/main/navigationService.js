import {StackActions } from '@react-navigation/routers';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigateAndReset(routeName, params) {
  _navigator.dispatch(
    StackActions.reset({
      index: 0,
      actions: [
        StackActions.navigate({
          routeName,
          params,
        }),
      ],
    })
  );
}

// add other navigation functions that you need and export them

export default {
  setTopLevelNavigator,
  navigateAndReset,
};