import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AboutUsScreen from '../Screens/more/About_us'
import DrawerMenu from './DrawerNav'
import { InfoIcon } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
// import { Icon } from 'native-base';

const Tab = createBottomTabNavigator();

export function BottomTabNav() {
    return (
      <Tab.Navigator screenOptions={({ route }) => ({
     
          activeTintColor: '#FFF',
          tabBarStyle: {
            backgroundColor: '#000',
            borderTopColor: 'transparent',
            
          },
        
        
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
     
          if (route.name === 'Home') {
            iconName = focused
              ? 'home'
              : 'home';  
          }

         else if (route.name === 'About_us') {
            iconName = focused
              ? 'people'
              : 'people';
          }
          else {
            iconName = focused ? 'ellipsis-vertical-sharp' : 'ellipsis-vertical-sharp';
          }
          return  <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
        tabBarOptions={{
          activeTintColor: '#FFF',
          inactiveTintColor: 'gray',
        }}
        >
        <Tab.Screen name="Home" component={DrawerMenu} 
        options={({ navigation }) => ({
                    headerShown: false,
                    title: 'My home',
                    headerStyle: {
                        backgroundColor: '#000',
                    },
                    headerLeft: () => (<InfoIcon  size={40} color="red" onPress={() => navigation.dispatch(DrawerActions.openDrawer())} />),
                    headerTintColor: '#fff',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                    },
                })} 
                />
         <Tab.Screen 
          name="About_us" component={AboutUsScreen}
          options={({ navigation }) => ({
                    headerShown: false,
                    title: 'About Us',
                    headerStyle: {
                        backgroundColor: '#000',
                    },
                })}/>   
      </Tab.Navigator>
    );
  }