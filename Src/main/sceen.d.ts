type StackParamList = {
    Splash: undefined;
    SignIn: undefined;
    SignUp: undefined
}

type RootStackParamList = {
    Home: undefined;
    DrawerHome:undefined,
    
}


type DrawerNavParamList = {
    Home: undefined;
    DrawerHome:undefined,
    MyProjectDetails:undefined,
    MyProject:undefined,
    OtpVerification:undefined
}

type tabBarOptions = {
    activeTintColor: string;
    inactiveTintColor: string;
}


type BottomNavParamList = {
    Home: undefined;
    DrawerHome:undefined
}
