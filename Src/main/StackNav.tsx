import * as React from 'react';
import { DrawerActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { BottomTabNav } from './BottomTabNav'

// import SplashScreen from '../Screens/Intro/SplashScreen';
// import LoginScreen from '../Screens/auth/LoginScreen';
// import IntroScreen from '../Screens/Intro/IntroScreen';
// import RegisterScreen from '../Screens/auth/RegisterScreen';
import LogoutScreen from '../Screens/auth/LogoutScreen';
import ProfileScreen  from '../Screens/Profile/ProfileScreen';
// import OtpVerification from '../Screens/auth/OtpVerification';
// import ForgetEmail from '../Screens/auth/ForgetEmail';
// import ForgetPassword from '../Screens/auth/ForgetPassword';

const Stack = createStackNavigator();
export function StackNav() {
    return (
        <Stack.Navigator
            initialRouteName="HomeScreen"

            screenOptions={{
                headerShown: false,

                headerStyle: {
                    backgroundColor: '#f4511e',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }}
        >
            {/* <Stack.Screen name="SplashScreen" component={SplashScreen} />
            <Stack.Screen name="IntroScreen" component={IntroScreen} />*/}
            <Stack.Screen name="ProfileScreen" component={ProfileScreen} /> 

            <Stack.Screen
                name="HomeScreen" component={BottomTabNav}/>
            
            {/* <Stack.Screen name="LoginScreen" component={LoginScreen} />
            <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
            <Stack.Screen name="OtpVerification" component={OtpVerification} />
            <Stack.Screen name="ForgetEmail" component={ForgetEmail} />
            <Stack.Screen name="ForgetPassword" component={ForgetPassword} />*/}
            <Stack.Screen name="Logout"
                component={LogoutScreen} /> 
        </Stack.Navigator>

    );
}