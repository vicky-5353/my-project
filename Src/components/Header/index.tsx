import React from 'react'
import PropsType from 'prop-types'
import { Alert, StatusBar, StyleSheet, View,SafeAreaView, Platform } from 'react-native'
// import { SafeAreaView } from 'react-native-safe-area-context'
import { Icon, Text,Pressable, ChevronLeftIcon, Menu, HamburgerIcon } from 'native-base'
import { DrawerActions, useNavigationState } from '@react-navigation/native';
import { useNavigation } from '@react-navigation/native';
// import FocusAwareStatusBar from '../StatusBar'
import CustomStatusBar from '../CustomStatusBar'
import { Colors } from '../../Themes';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
// import { SafeAreaView } from 'react-native-safe-area-context';
// import * as RootNavigation from '../../../main/RootNavigation';
// RootNavigation.navigate('Home')


interface data {
    iconName?: any; //useing "?" for optional
    iconType?: any;
    headerTitle?: string;
    iconStyle?: object;
    headerTtileStyle?: object;
    backgroundColor:any,
    isVisibleFilter:false,
    isAppliedFilter:number,
    onPressFilter:()=>{}

}
const Headers = (props: data) => {
    const navigation = useNavigation()
    let iconName = props.iconName || "menu"
    let iconType = props.iconType || "Ionicicons"
    let iconStyle = props.iconStyle
    let headerTtileStyle = props.headerTtileStyle

    const state = useNavigationState(state => state);
    const routeName = (state.routeNames[state.index]);
    let headerTitle = props.headerTitle || routeName
    let bgColor=props.backgroundColor??Colors.HEADER

   const goBack=()=>{
    //    console.log(3333)
      navigation.goBack()
   }

    return (

      <View >
        <CustomStatusBar  backgroundColor={Colors.STATUS_BAR} barStyle="light-content" />
             <View style={[styles.header,{backgroundColor:bgColor,}]}>
                <View style={{flexDirection:'row',alignItems:'center',height:'100%'}}>
                {routeName == "Home" ?
                <Pressable onPress={() => {
                        navigation.dispatch(DrawerActions.openDrawer())
                    }}
                     style={[styles.icon_style,iconStyle]} >
                      <HamburgerIcon color="#fff"/>
                    </Pressable>
                     :
                    <Pressable onPress={() =>goBack()} style={[styles.icon_style,iconStyle]} >
                      <ChevronLeftIcon color="#fff"  />
                    </Pressable>
                  
                }
                <View style={{height:'100%',justifyContent:'center',marginLeft:10}}>
                <Text style={[styles.headerTitleStyle, headerTtileStyle]}>{headerTitle}</Text>
                </View>
                
                </View>
            </View>
            
        </View>
        
    )
}
Headers.PropsType = {
    headerTitle: PropsType.string
}
export default Headers
const styles = StyleSheet.create({
    header: {
        height: 55,
        backgroundColor: Colors.HEADER,
        flexDirection: 'row',
        alignItems: 'center',
        elevation:10,
        // alignContent:'center',
        marginBottom:Platform.OS=="ios"?5:0,
        justifyContent:'space-between',


    },
    headerTitleStyle: {
        color: '#FFF',
        fontSize: 20,
        paddingTop:5,
        alignSelf: 'center',
    },
    icon_style: {
        color: '#fff', 
        fontSize:30, 
        // marginLeft: 15,
        width:50,
        height:'100%',
        alignSelf:'center',
        // backgroundColor:'red',
        justifyContent:'center',
        alignItems:'center'

    
        
    },
})