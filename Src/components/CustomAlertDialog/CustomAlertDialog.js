import React from "react";
import { AlertDialog, Button, Center } from "native-base";
const CustomAlertDialog = ({
  isVisible = false,
  headerTitle = "",
  description = "",
  onClose,
  onSubmit,
  buttonTxt1 = "",
  buttonTxt2 = "Ok",
  buttonType2 = "danger",
  buttonType1 = "info",
  variantType1=""
}) => {
  const cancelRef = React.useRef();
  return (
    <Center>
      <AlertDialog
        leastDestructiveRef={cancelRef}
        isOpen={isVisible}
        onClose={onClose}
      >
        <AlertDialog.Content>
          {headerTitle ? (
            <>
              <AlertDialog.CloseButton />
              <AlertDialog.Header>{headerTitle}</AlertDialog.Header>
            </>
          ) : null}
          <AlertDialog.Body>{description}</AlertDialog.Body>

          <AlertDialog.Footer>
            <Button.Group space={2}>
              <Button
                variant={variantType1}
                colorScheme={buttonType2}
                onPress={variantType1=="unstyled"?null:onClose}
                ref={cancelRef}
              >
                {buttonTxt1}
              </Button>
              <Button colorScheme={buttonType1} onPress={onSubmit}>
                {buttonTxt2}
              </Button>
            </Button.Group>
          </AlertDialog.Footer>
        </AlertDialog.Content>
      </AlertDialog>
    </Center>
  );
};

export default CustomAlertDialog;
