import {Toast } from "native-base";

export const showToast=(text,type,buttonText,textStyle,buttonStyle,position,onClose,style)=>
{
  
    text=text || "Toast Message!"
    buttonText =buttonText || ""
    type= type || "danger" //danger, success, warning
    textStyle =textStyle || null
    buttonStyle =buttonStyle || null
    position=position||'bottom'

    Toast.show({
        text: text,
        type : type,
        buttonText: buttonText,
        textStyle :textStyle,
        buttonStyle: buttonStyle,
        position:position,
        onClose:onClose,
        duration:onClose?1000:800,
        style:style
        // style:{borderRadius:50}
      })
}

export const hideToast=()=>
{
  Toast.hide();
}