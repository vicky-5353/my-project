import moment from "moment";



export const getConferenceScheduleTime=(data,settingData )=>{
    if(settingData){
    var d = new Date();
    var currentTime = moment(d,'HH:mm:ss').format('HH:mm:ss');
    var StartTime = data.from_time 
    var EndTime = data.to_time 
    var startBeforeMinute = settingData[5].CONFERENCE_START_BEFORE;
    var Grace_time = settingData[6].CONFERENCE_GRACE_TIME;
    var newStartTime = moment(StartTime,'HH:mm:ss').subtract(startBeforeMinute,'minutes').format('hh:mm:ss a');
    var newEndTime = moment(EndTime,'HH:mm:ss').add(Grace_time,'minutes').format('hh:mm:ss a');
    return {conference_startTime:newStartTime,conference_endTime:newEndTime};
    }else{
        return {conference_startTime:"",conference_endTime:""};
      
    }
}


export const validateInputText = (value) => {
    if (value == undefined || value == null || value == "") {
        return true;
    }
    else if (value && value.toString().trim().length == 0)
        return true;

    else
        return false
}

export const validateInputField = (value, msg) => {
    if (value == undefined || value == null || value == "") {
        return msg;
    }
    else if (value && value.toString().trim().length == 0)
        return msg;

    else
        return ""
}



export const validateInputMobile = (value,msg) => {
    if (value == undefined || value.trim() == "")
        return msg;
    else if (value != undefined && value.trim() != "" && value.length > 0 && value.length < 10)
        return msg;
    else
        return ""
}

export const isValidPostCode = (value) => {
    if (value == undefined || value.trim() == "")
        return "Please Enter valid post code";
    else if (value != undefined && value.trim() != "" && value.length > 0 && value.length < 5)
        return "Please Enter valid post code";
    else
        return ""
}
export const numberFormat1 = (text) => {
    return text.replace(/[^+\d]/g, '');
}
export const numberFormat = (value) => {
    value.replace(/[- #*;,.<>\{\}\[\]\\\/]/gi, '')
}
export const isRequieredField = (params) => {
    let errorMsg = null;
    if (params) {
        for (var key in params) {
            if (params[key] && params[key].length > 0) {
                errorMsg = params[key];
                break;
            }
        }
    }
    return errorMsg
}

export const phoneIsValid = (phone) => {
    if (phone) {
        let indianMobileRegex = /^[6-9]{1}[0-9]{9}$/;
        return indianMobileRegex.test(String(phone));
    } else {
        return false
    }
};
export const emailIsValid = (email) => {
    if (email) {
        let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailRegex.test(email.toString().toLowerCase());
    }
    else {
        return false
    }
};

export const passwordIsValid=(password)=>{
    if(password){
        let regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
       return regex.test(password)
    }else{
        return false
    }
}


export const isEmptyField = (params) => {
    let isEmpty = false;
    if (params) {
        for (var key in params) {
            if (params[key] == undefined || params[key].trim() == "") {
                isEmpty = true;
                break;
            }
        }
    }
    return isEmpty
}
