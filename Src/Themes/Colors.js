const Colors = {
    TRANSPARENT:'transparent',
    WHITE:'#FFFFFF',
    BLACK:"#000000",
    LIGHT_BLUE:'#3A9DF9',
    DARK_BLUE:'#2E75B6',
    DARK_GRAY:'#706C6C',
    GRAY:'#979797',
    SILVER:'#CCCCCC',
    LIGHT_GRAY:'#DADADA',
    STATUS_BAR:'#3A9DF9',
    HEADER:'#2E75B6',
    DARK_PINK:'#cb29ac'

}
export default Colors
