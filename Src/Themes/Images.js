const images = {
	conference_image: require('../assets/images/1-xhdpi.png'),
    paper_submit_image: require('../assets/images/2-xhdpi.png'),
    login_bg_image:require('../assets/images/new_login_bg.jpg'),
    signup_bg_image:require('../assets/images/new_signup_bg.jpg'),
    mentor_image: require('../assets/images/3-xhdpi.png'),
    email_icon: require('../assets/images/carbon_email.png'),
    pass_key: require('../assets/images/carbon_password.png'),
    user_icon:require('../assets/images/ant-design_user-outlined.png'),
    phone_icon:require('../assets/images/clarity_phone-handset-line.png'),
    logo:require('../assets/images/new_logo.png'),
    otp_icon:require('../assets/images/Mobile_otp_1.png'),
    forgot_email_icon:require('../assets/images/forgot-email-icon.png'),
    forget_icon:require('../assets/images/forgot-password-icon.png'),
    location:require('../assets/images/fluent_location-48-regular.png'),
    email_line_icon:require('../assets/images/clarity_email-line.png'),
    organizer_icon:require('../assets/images/cil_user.png'),
    venue_icon:require('../assets/images/whh_placeios.png'),
    cash_icon:require('../assets/images/vaadin_cash.png'),
    group_people_icon:require('../assets/images/clarity_employee-group-line.png'),
    organization:require('../assets/images/ic_outline-subtitles.png'),
    rating:require('../assets/images/star.png'),
    person_icon:require('../assets/images/akar-icons_person.png'),
    team_icon:require('../assets/images/fluent_people-team-20-regular.png'),
    pen_icon:require('../assets/images/system-uicons_write.png'),
    download_icon:require('../assets/images/tray-arrow-down.png'),
    comment_icon:require('../assets/images/la_comments-solid.png'),
    comment_edit_icon:require('../assets/images/bx_bx-message-edit.png'),
    comment_delete_icon:require('../assets/images/ri_chat-delete-line.png'),
    comment_check_icon:require('../assets/images/mdi-light_check.png'),
    upload_icon:require('../assets/images/ant-design_upload-outlined.png'),
    profile_user_icon:require('../assets/images/ci_user.png'),
    facebook_icon:require('../assets/images/Facebook.png'),
    degree_icon:require('../assets/images/mdi_book-education-outline.png'),
    book_icon:require('../assets/images/ph_books-thin.png'),
    education_icon:require('../assets/images/carbon_education.png'),
    create_icon:require('../assets/images/grommet-icons_projects.png'),
    

    exp_calender_icon:require('../assets/images/healthicons_i-schedule-school-date-time-outline.png'),
    about_icon:require('../assets/images/akar-icons_person.png'),
    skills_icon:require('../assets/images/ph_books-thin.png'),
    education_icon:require('../assets/images/carbon_education.png'),
    experience_icon:require('../assets/images/healthicons_i-schedule-school-date-time-outline.png')

};

export default images;
