import { Text, View } from 'react-native'
import React, { Component } from 'react'
import Headers from '../../components/Header';
export default class LogoutScreen extends Component {
  render() {
    return (
      <View>
        <Headers headerTitle="Log Out" />
        <Text>LogoutScreen</Text>
      </View>
    )
  }
}