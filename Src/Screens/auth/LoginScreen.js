import React, { Component } from 'react'
import { Text, View,TextInput,SafeAreaView,StyleSheet,Image,TouchableOpacity,ImageBackground,Modal, StatusBar} from 'react-native'
import { KEY, setValue,getValue} from '../../storage/AsyncStorageData';
import { Images,Colors } from '../../Themes';
import Icon from 'react-native-vector-icons/FontAwesome';
import { RootState } from '../../redux/store/Store';
import { saveUserData, updateEmail, updatePassword, updateUserName } from '../../redux/actions/AuthAction';
import { connect } from 'react-redux';
import { emailIsValid, passwordIsValid } from '../../helpher/Utils';
import FullScreenLoader from '../../components/FullScreenLoader';
import CustomAlertDialog from '../../components/CustomAlertDialog/CustomAlertDialog';
import { Endpoints } from '../../config/Server';
import * as Services from "../../services/genericService";
import { StackActions } from '@react-navigation/routers';

class LoginScreen extends Component {
    constructor(props) {
        super(props);
        // this.register= this.register.bind(this);
        
        this.state={
            email_id:'',
            password:'',
            icon: 'eye-slash',
            hidePassword: true,
            visible: false,
            mailerror:'none',
            pwderror:'none',
        }
    }
    componentDidMount=()=>{
        // this.props.updateEmail('')
        // this.props.updatePassword('')
    }   
    
    _validate = async () =>{
        const{email,password}=this.state;
        let isValidEmail=emailIsValid(email)
        // let uniqueId = DeviceInfo.getUniqueId();
        // let systemName = DeviceInfo.getSystemName();
        // const fcm_token = await getValue(KEY.FCM_TOKEN);
        // console.log("Fcm Token:",fcm_token)
        //console.log(email,password)
        if(isValidEmail && password){
            let param={email,password}
            this._login(param)
        }else if(!isValidEmail && !password){
            this.setState({mailerror:'flex',pwderror:'flex'});
        }
        else if(!isValidEmail){
            this.setState({mailerror:'flex',pwderror:'none'});
        }else if(!password){
            // this.showAlertMsg("Validation Error!","Please Enter valid email and password")
            this.setState({pwderror:'flex',mailerror:'none'});
        }
    }

    _login = (param) => {
        this.setState({ isLoading: true });
        // Services.postGenericService(Endpoints.login, param)
        //   .then((res) => {
        //     if (res) {
        //       const { status, message, data,token } = res;
        //       if (status) {
        //         data.token=token
        //         this.getNotification();
                let data={email:this.state.email};
                this.props.saveUserData(data)
        //           } else {
        //         this.showAlertMsg("some error occurred!", message);
        //       }
        //     } else {
        //       this.showAlertMsg("something went wrong", "");
        //     }
        //   })
        //   .catch((errMsg) => {
        //     console.log(errMsg);
        //     this.showAlertMsg("something went wrong", "");
        //   })
        //   .finally(() => {
        //     this.setState({ isLoading: false });
        //   });
      };

    //   _inputValue=(value,type)=>{
    //     switch(type){
    //         case 'EMAIL':
    //             this.props.updateEmail(value);
    //             break;
    //         case 'PASSWORD':
    //             this.props.updatePassword(value);
    //             break;    
    //         }
    //     };
        
    showAlertMsg = (title, description) => {
        this.setState({
            isVisibleAlert: true,
            alertMsg: description,
            alertTitle: title,
        });
          }
      
    updatePasswordVisiblity = () => {
        this.setState(prevState => ({
            icon: prevState.icon === 'eye' ? 'eye-slash' : 'eye',
            hidePassword: !prevState.hidePassword,
        }));
    };      
    render() {
        const{email,password}=this.state;
        const{isVisibleAlert,isLoading,alertTitle,alertMsg,hidePassword,icon,mailerror,pwderror,}=this.state
    return (
        <SafeAreaView style={styles.container}>

        <StatusBar
                animated={true}
                backgroundColor={Colors.LIGHT_BLUE}
                barStyle={"light-content"}
                // showHideTransition={"fade"}
                hidden={false} />
                        <FullScreenLoader isLoading={isLoading} />
                            <CustomAlertDialog
                                isVisible={isVisibleAlert}
                                onClose={() => this.setState({ isVisibleAlert: false })}
                                onSubmit={() =>this.setState({ isVisibleAlert: false })}
                                headerTitle={alertTitle}
                                description={alertMsg}
                                //   buttonTxt1="Cancel"
                                variantType1="unstyled"
                                buttonTxt2="Ok"
                                />
                            <ImageBackground source={Images.login_bg_image} style={{height:'100%',width:'100%',justifyContent:'center'}}>
                                <View style={{paddingVertical:20,width:"75%",backgroundColor:'white',alignSelf:'center',marginTop:"20%"}}> 
                                <Text style={{color:'#2E75B6',fontSize:25,fontWeight:'bold',alignSelf:'center',}}>Welcome back!</Text>
                                {/* <Text style={{color:Colors.BLACK,fontSize:15,fontWeight:'500',alignSelf:'center',paddingTop:10,textAlign:'center'}}>Log In to your existant account of ULBHA</Text> */}
                            </View>
                            <View style={[styles.sectionStyle,]} >
                                <Image source={Images.email_icon}style={styles.imageStyle}/>
                                <TextInput
                                    style={{flex: 1, color: Colors.BLACK,fontSize:15,}} 
                                    placeholder="Email"
                                    placeholderTextColor={Colors.DARK_GRAY}
                                    autoCorrect={false}
                                    value={email}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text)=> this.setState({email:text})}/>
                            </View>
                            <Text style={{fontSize:14,color:'red',fontWeight:'500',marginStart:25,marginBottom:2,display:mailerror}}>Please Enter valid email</Text>
                            <View style={[styles.sectionStyle]}>
                                <Image source={Images.pass_key} style={styles.imageStyle}/>
                                <TextInput
                                    secureTextEntry={hidePassword}
                                    style={{flex: 1,color:Colors.BLACK,fontSize:15,}}
                                    placeholder="Password"
                                    placeholderTextColor={Colors.DARK_GRAY}
                                    autoCorrect={false}
                                    value={password}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text)=> this.setState({password:text})}/>
                                <Icon name={icon} style={[styles.imageStyle,{color:Colors.DARK_BLUE}]} onPress={this.updatePasswordVisiblity}></Icon>   
                            </View>
                            <Text style={{fontSize:14,color:'red',fontWeight:'500',marginStart:25,marginBottom:2,display:pwderror}}>Please Enter valid password</Text>
                            <TouchableOpacity onPress={this._validate} style={{backgroundColor:'#2E75B6',height:55,width:'90%',alignSelf:'center',marginTop:5,
                                        borderRadius:5,justifyContent:'center'}}>
                                    <Text style={{color:'white',textAlign:'center',fontSize:18}}>Log In</Text>
                            </TouchableOpacity>
                            {/* <View style={{flexDirection: 'row',justifyContent: 'space-between',}}>
                                <TouchableOpacity style={{marginTop:10,justifyContent:'center',marginLeft: 20,}} 
                                        onPress={this.forgotpassword}>
                                    <Text style={{color:'#2E75B6',textAlign:'center',fontSize:16,fontWeight:'500'}}>Forgot Password?</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{marginTop:10,justifyContent:'center',marginRight: 20,}} 
                                        onPress={this.forgotemail}>
                                    <Text style={{color:'#2E75B6',textAlign:'center',fontSize:16,fontWeight:'500'}}>Forgot Email?</Text>
                            </TouchableOpacity>
                            </View> */}
                            {/* <View style={{height:'4%',width:'80%',alignSelf:'center',flexDirection: 'row',bottom:50,
                                                position:'absolute',justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:15,color:Colors.BLACK,fontWeight:'500'}}>Not a user yet? </Text>
                                <TouchableOpacity style={{alignSelf:'center',justifyContent:'center'}} 
                                        onPress={this.register}>
                                    <Text style={{color:'#2E75B6',textAlign:'center',fontSize:15,fontWeight:'bold'}}>Sign Up</Text>
                                </TouchableOpacity>
                                <Text style={{fontSize:15,fontWeight:'500',color:Colors.BLACK}}> for ULBHA</Text>
                            </View> */}
                            
                        </ImageBackground>             
                    </SafeAreaView>
                )
            }
    }
    const mapStateToProps = (state) => {
        return {
            token: state.auth.token,
            username: state.auth.username,
            password:state.auth.password,
            email:state.auth.email
        }
    }
    
        const mapDispatchToProps = (dispatch) => {
            return {
                updateEmail:(data)=>dispatch(updateEmail(data)),
                updatePassword:(data)=>dispatch(updatePassword(data)),
                saveUserData:data=>dispatch(saveUserData(data))
            }
        }
    
    export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
    
    
    const styles = StyleSheet.create({
        container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor:Colors.LIGHT_BLUE
        },
        sectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'r',
        borderWidth: 1.5,
        borderColor: '#3A9DF9',
        height: 55,
        borderRadius: 5,
        margin: 5,
        marginLeft: 20,
        marginRight: 20,
        },
        imageStyle: {
        padding: 10,
        margin: 5,
        fontSize:20,
        tintColor:'#2E75B6',
        resizeMode: 'stretch',
        alignItems: 'center',
        },
    
        modalEmailStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'r',
        borderWidth: 0.2,
        height: 55,
        borderRadius: 2,
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        },
        modalImageStyle: {
        padding: 10,
        marginLeft: 15,
        tintColor:'#979797',
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center',
        },
    });


