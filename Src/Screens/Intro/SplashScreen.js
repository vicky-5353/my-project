import React, {Component} from 'react';
import {View, Text,StatusBar, SafeAreaView, StyleSheet,Image, Alert, Platform} from 'react-native';
import { Colors, Images } from '../../Themes';
import { connect } from 'react-redux';
import { getUserData, saveUserData, saveUserId } from '../../redux/actions/AuthAction';
import { getValue, KEY,setValue } from '../../storage/AsyncStorageData';
import { StackActions } from '@react-navigation/routers';
import { Endpoints } from '../../config/Server';
import * as Services from "../../services/genericService";


// import notifee from '@notifee/react-native';
// import {
//   notifications,
//   messages,
//   NotificationMessage,
//   Android
// } from "react-native-firebase-push-notifications";
class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      hasPermission: false
    }
  }

 
 componentDidMount = async () =>{
  let {navigation} = this.props;
  let userInfo=await getValue(KEY.USER_DATA)

if(userInfo){
  let userData=JSON.parse(userInfo)
  console.log(userInfo)
  const{email,id,password,token}=userData;
  this.props.saveUserData(userData);
  if(email){
    // setTimeout(() => {
  
    //   // this.getNotification ();
    // }, 2000);

  }else{
    setTimeout(() => {
     navigation.dispatch(StackActions.replace('LoginScreen'));
    navigation.navigate('LoginScreen');
  }, 2000);
  }
    
  }
  else{
    setTimeout(() => {
      navigation.dispatch(StackActions.replace('LoginScreen'));
     this.props.navigation.navigate('LoginScreen');
    }, 1000); 
  }

}

  render() {
    return (
      <SafeAreaView style={styles.maincontainer}>
          {/* <StatusBar
        animated={true}
        backgroundColor={Colors.LIGHT_BLUE}
        barStyle={"light-content"}
        // showHideTransition={"fade"}
        hidden={false} /> */}
        <Image source={Images.logo} style={{alignSelf: 'center',height:"15%",width:'100%'}}></Image>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user_id: state.auth.user_id,

  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    // getUserData:()=>dispatch(getUserData()),
    saveUserData:(data)=>dispatch(saveUserData(data)),
    saveUserId:id=>dispatch(saveUserId(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen)

const styles = StyleSheet.create({
  maincontainer: {
    flex: 1,
    // backgroundColor: Colors.LIGHT_BLUE,
    justifyContent:'center'
  },
});

// export default SplashScreen;
