import { Text, View } from 'react-native'
import React, { Component } from 'react'
import Headers from '../../components/Header';
export default class MainScreen extends Component {
  render() {
    return (
      <View>
        <Headers />
        <Text>MainScreen</Text>
      </View>
    )
  }
}