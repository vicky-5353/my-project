import { configureStore } from '@reduxjs/toolkit'
import { fromPairs } from 'lodash';
import authReducers from '../reducers/AuthReducers';
import settingReducer from '../reducers/UserSettingReducers';
import filterReducer from '../reducers/FilterDataReducers';
const store = configureStore({
    reducer: {
        auth: authReducers,
        setting: settingReducer,
        filter: filterReducer,
    },
  })

  // Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export default store;
  