import { FILTERDATA, CONFFILTERDATA, PROJECTFILTERDATA } from '../actions/ActionTypes';



const initialState = {
    filterData:[],
    conffilterData:[],
    projectfilterData:[],
}

export default (state = initialState, action:any) => {
  switch (action.type) {

    case FILTERDATA.UPDATE_FILTER_DATA:
      return {
        ...state,
        filterData: action.payload
      };
    case CONFFILTERDATA.UPDATE_CONF_FILTER_DATA:
      return {
        ...state,
        conffilterData: action.payload
      };
    case PROJECTFILTERDATA.UPDATE_PROJECT_FILTER_DATA:
      return {
        ...state,
        projectfilterData: action.payload
      };    
 
     default:
      return state
  }
};