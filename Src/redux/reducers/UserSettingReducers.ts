import { USERSETTING } from '../actions/ActionTypes';



const initialState = {
    userSettingData:[]
}

export default (state = initialState, action:any) => {
  switch (action.type) {

    case USERSETTING.UPDATE_SETTING_DATA:
      return {
        ...state,
        userSettingData: action.payload
      };
 
     default:
      return state
  }
};
