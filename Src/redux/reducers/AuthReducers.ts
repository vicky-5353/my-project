
import AsyncStorage from '@react-native-community/async-storage';
import { KEY } from '../../storage/AsyncStorageData';
import { LOCALSTORAGE, LOGIN } from '../actions/ActionTypes';
const initialState = {
username:"",
password:"",
user_data:null,
type:null,
token:null,
email:null,
user_id:0
}

export default (state = initialState, action:any) => {
  switch (action.type) {

    case LOGIN.UPDATE_USERNAME:
      return {
        ...state,
        username: action.payload
      };
    case LOGIN.UPDATE_PASSWORD:
      return {
        ...state,
        password: action.payload
      };
    case LOGIN.UPDATE_USERID:
        return {
          ...state,
          user_id: action.payload
        };
      
    case LOGIN.UPDATE_EMAIL:
      return {
        ...state,
        email: action.payload
      };  
    case LOGIN.LOGIN_SUCCESS:
        return {
          ...state,
          user_data: action.data,
          type:action.type,
          
  };
    case LOGIN.SAVE_AUTHORIZED_DATA:
      AsyncStorage.setItem(KEY.USER_DATA, JSON.stringify(action.payload));
      return {
        ...state,

        token: action.payload.token,
        user_id:action.payload.id,
        mobile_number:action.payload.mobile_number,
        email:action.payload.email,
        first_name:action.payload.name,
        name:action.payload.name,
        last_name:action.payload.last_name

      };

    case LOGIN.UPDATE_AUTHORIZED_DATA:
      return {
        ...state,
        username: action.payload.email,
        mobile: action.payload.mobile,
        token:action.payload.token,
        user_data:action.payload
      
      };

    default:
      return state
  }
};
