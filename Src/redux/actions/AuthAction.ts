import { LOGIN, LOCALSTORAGE } from './ActionTypes';
import AsyncStorage from '@react-native-community/async-storage';
import * as Services from "../../services/genericService";
import {Endpoints} from '../../config/Server'
import { KEY } from '../../storage/AsyncStorageData';



export const loginApi = (param:loginProps) => {
    return (dispatch:any, getState:any) => {
        dispatch({ type: LOGIN.LOGIN_PENDING, message: "" });
      Services.postGenericService(Endpoints.login,param)
        .then(response => {
          dispatch({
            type: LOGIN.LOGIN_SUCCESS,
            message:response.message,
            data:response
          });
        })
        .catch(errMsg => {
          dispatch({ type: LOGIN.LOGIN_FAIL, message: errMsg });
        });
    };
  };


export const updateUserName = (username:string) => {
    return {
        type: LOGIN.UPDATE_USERNAME,
        payload:username
    }
}
export const updateEmail = (email:string) => {
    return {
        type: LOGIN.UPDATE_EMAIL,
        payload:email
    }
}
export const updatePassword = (password :string)=> {
    return {
        type: LOGIN.UPDATE_PASSWORD,
        payload:password
    }
}

export const saveUserData=(payload:userProps)=> {
    return {
        type: LOGIN.SAVE_AUTHORIZED_DATA,
        payload:payload
    }
}
export const saveUserId=(payload:userProps)=> {
    return {
        type: LOGIN.UPDATE_USERID,
        payload:payload
    }
}



export function getUserData() {
    return (dispatch:any) => {
        AsyncStorage.getItem(KEY.USER_DATA)
        .then((data:any) => {
            debugger
            const ldata=JSON.parse(data)
            dispatch(saveUserData(ldata))
            dispatch(updateUserName(ldata.email))
            return {
                type: LOGIN.UPDATE_AUTHORIZED_DATA,
            }
        })
        .catch((err) => {
        })
    }
}

export function removeUserData(){
    return (dispatch:any) => {
        AsyncStorage.removeItem(KEY.USER_DATA);
    }
}

export function getLoginUserData() {
    return (dispatch:any) => {
        AsyncStorage.getItem(KEY.USER_DATA)
        .then((data:any) => {
            var dataJSON = JSON.parse(data);
            dispatch(updateUserName(dataJSON.userName));
            // dispatch(updateAuthorizationData(JSON.parse(data)));
        })
        .catch((err) => {
        })
    }
}
