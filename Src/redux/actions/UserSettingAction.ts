import { USERSETTING } from './ActionTypes';

export const updateSettingData = (settingData:any) => {
    return {
        type: USERSETTING.UPDATE_SETTING_DATA,
        payload:settingData
    }
}