import { FILTERDATA, CONFFILTERDATA, PROJECTFILTERDATA } from './ActionTypes';

export const updatefilterData = (filterData:any) => {
    return {
        type: FILTERDATA.UPDATE_FILTER_DATA,
        payload:filterData
    }
}

export const updateconffilterData = (conffilterData:any) => {
    return {
        type: CONFFILTERDATA.UPDATE_CONF_FILTER_DATA,
        payload:conffilterData
    }
}

export const updateprojectfilterData = (projectfilterData:any) => {
    return {
        type: PROJECTFILTERDATA.UPDATE_PROJECT_FILTER_DATA,
        payload:projectfilterData
    }
}