import AsyncStorage from '@react-native-community/async-storage';
// import * as HelpherFile from '@storage/HelpherFile'

export const KEY={
USER_DATA:'USER_DATA',
AUTH_TOKEN:'AUTH_TOKEN',
FCM_TOKEN:'FCM_TOKEN', 
NOTIFICATION:'NOTIFICATION',  
}


export const setValue = async (key,value) => {
    let status=false
    try {
      await AsyncStorage.setItem(key, value)
      status=true
    } catch(e) {
      console.log(e)
      status=false
    }
    return status
  }


  export const getValue = async (key) => {
    try {
      let data=  await AsyncStorage.getItem(key)
      if(data==null)
      return ''
    else
      return  data
    } catch(e) {
      console.log(e)
      return ''
    }

  }

export const getAllKeys = async () => {
    let keys = []
    try {
      keys = await AsyncStorage.getAllKeys()
    } catch(e) {
      // read key error
    }
    console.log(keys)
    // example console.log result:
    // ['@MyApp_user', '@MyApp_key']
  }
  
  export const removeValue = async () => {
    try {
      await AsyncStorage.removeItem('CUST_ID')
      await AsyncStorage.removeItem('AUTH_TOKEN')
      await AsyncStorage.removeItem('CUSTOMER_NAME')
      await AsyncStorage.removeItem('CUSTOMER_MOBILE_NO')
      await AsyncStorage.removeItem('CUST_INFO')
      await AsyncStorage.removeItem('CUSTOMER_EMAIL')
      await AsyncStorage.removeItem('DEFAULT_ADDRESS')
      await AsyncStorage.removeItem('CART_DATA')
      await AsyncStorage.removeItem('REST_DATA')
      return true
    } catch(e) {
      return false
    }
    console.log('Done.')
  }



  export const removeAyncDataByKey = async (key) => {
    try {
      await AsyncStorage.removeItem(key)
      return true
    } catch(e) {
      // remove error
      return false
    }
    console.log('Done.')
  }


  